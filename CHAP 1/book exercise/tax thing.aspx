﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tax thing.aspx.cs" Inherits="tax_thing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>401k future value calculator</h1>
            <table>
                <tr>
                    <td>Monthly Investment</td>
                    <td><asp:DropDownList ID="ddlMonthlyInvestment" runat="server" Width="106px"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Annual interest rate</td>
                    <td><asp:TextBox ID="txtInterestRate" runat="server" Width="100px">3.0</asp:TextBox></td>
                </tr>
                <tr>
                    <td>Number of years</td>
                    <td><asp:TextBox ID="txtYears" runat="server" Width="100px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Future Value</td>
                    <td><asp:Label ID="lblFutureValue" runat="server" Font-Bold="true"></asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><asp:Button ID="buttonCalculate" runat="server" Text="Calculate" Width="100px" OnClick="btnCalculate_Click" /></td>
                    <td><asp:Button ID="btnClear" runat="server" Text="Clear" Width="100px" OnClick="btnClear_Click" CausesValidation="false" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
