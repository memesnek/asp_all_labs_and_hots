﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cart : System.Web.UI.Page
{
    private int _programPrice;
    private int _toolkitPrice;
    private int _totalPrice;

    //public bool ValidateDate()
    //{
    //    string selectedDate = orderDateBox.Text;
    //int selectedMonth = Convert.ToInt32(selectedDate.Substring(5, 2));
    //int selectedDay = Convert.ToInt32(selectedDate.Substring(8, 2));
    //int selectedYear = Convert.ToInt32(selectedDate.Substring(0, 4));


    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            toolkitList.SelectedIndex = 0;
            programDdl.SelectedIndex = 0;
        }
    }

    protected void submitBtn_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            double programCost = Convert.ToDouble(programDdl.SelectedValue);
            double toolkitCost = Convert.ToDouble(toolkitList.SelectedValue);

            DateTime orderDate = Convert.ToDateTime(orderDateBox.Text);
            DateTime deliveryDate = orderDate.AddDays(3);

            CostLbl.Text = "Cost: " + (programCost + toolkitCost).ToString("C");
            deliveryDateLbl.Text = "Delivery Date: " + deliveryDate.ToShortDateString();
            //deliveryDateLbl.Text = "Delivery Date: " + deliveryMonth + "/" + deliveryDay + "/" + deliveryYear;

            ////calculate prices
            //switch (programDdl.SelectedIndex)
            //{
            //    case 1:
            //        _programPrice = 5000;
            //        break;
            //    case 2:
            //        _programPrice = 6000;
            //        break;
            //    case 3:
            //        _programPrice = 7000;
            //        break;
            //    default:
            //        break;
            //}
            //switch (toolkitList.SelectedIndex)
            //{
            //    case 1:
            //        _toolkitPrice = 5000;
            //        break;
            //    case 2:
            //        _toolkitPrice = 400;
            //        break;
            //    case 3:
            //        _toolkitPrice = 35;
            //        break;
            //    default:
            //        break;
            //}
            //_totalPrice = _programPrice + _toolkitPrice;

            ////find date
            //string selectedDate = orderDateBox.Text;
            //int selectedMonth = Convert.ToInt32(selectedDate.Substring(5, 2));
            //int selectedDay = Convert.ToInt32(selectedDate.Substring(8, 2));
            //int selectedYear = Convert.ToInt32(selectedDate.Substring(0, 4));
            //int deliveryMonth = selectedMonth;
            //int deliveryDay = selectedDay + 3;
            //int deliveryYear = selectedYear;
            //bool leapyear = false;
            //if ((deliveryYear % 4) == 0)
            //{
            //    if ((deliveryYear % 100) == 0)
            //    {
            //        if ((deliveryYear % 400) == 0)
            //        {
            //            leapyear = true;
            //        }
            //    }
            //}
            ////months with 30 days
            //if ((deliveryMonth == 6 || deliveryMonth == 9 || deliveryMonth == 11) && deliveryDay > 30)
            //{
            //    deliveryMonth += 1;
            //    switch (deliveryDay)
            //    {
            //        case 31:
            //            deliveryDay = 1;
            //            break;
            //        case 32:
            //            deliveryDay = 2;
            //            break;
            //        case 33:
            //            deliveryDay = 3;
            //            break;
            //        default:
            //            break;
            //    }
            //}
            ////months with 31 days
            //if ((deliveryMonth == 1 || deliveryMonth == 3 || deliveryMonth == 5 || 
            //    deliveryMonth == 7 || deliveryMonth == 8 || deliveryMonth == 10 || 
            //    deliveryMonth == 12) && deliveryDay > 31)
            //{
            //    deliveryMonth += 1;
            //    switch (deliveryDay)
            //    {
            //        case 32:
            //            deliveryDay = 1;
            //            break;
            //        case 33:
            //            deliveryDay = 2;
            //            break;
            //        case 34:
            //            deliveryDay = 3;
            //            break;
            //        default:
            //            break;
            //    }
            //}
            ////feburary non leap year
            //if (deliveryMonth == 2 && deliveryDay > 28 && !leapyear)
            //{
            //    deliveryMonth += 1;
            //    switch (deliveryDay)
            //    {
            //        case 29:
            //            deliveryDay = 1;
            //            break;
            //        case 30:
            //            deliveryDay = 2;
            //            break;
            //        case 31:
            //            deliveryDay = 3;
            //            break;
            //        default:
            //            break;
            //    }
            //}
            ////feburary leap year
            //if (deliveryMonth == 2 && deliveryDay > 29 && leapyear)
            //{
            //    deliveryMonth += 1;
            //    switch (deliveryDay)
            //    {
            //        case 30:
            //            deliveryDay = 1;
            //            break;
            //        case 31:
            //            deliveryDay = 2;
            //            break;
            //        case 32:
            //            deliveryDay = 3;
            //            break;
            //        default:
            //            break;
            //    }
            //}
            //if (deliveryMonth > 12)
            //{
            //    deliveryMonth = 1;
            //    deliveryYear += 1;
            //}

            ////Update labels
            //CostLbl.Text = "Cost: " + _totalPrice.ToString("C");
            //deliveryDateLbl.Text = "Delivery Date: " + deliveryMonth + "/" + deliveryDay + "/" + deliveryYear;
        }
    }
    public void CalcDeliveryDate()
    {

    }
}