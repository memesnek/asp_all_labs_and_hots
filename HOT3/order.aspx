﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="order.aspx.cs" Inherits="cart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StyleSheet.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="display:block; text-align:center;">Spring 2018 Semester Cost Estimation</h1>
        </div>
        <div>

            <asp:DropDownList ID="programDdl" runat="server">
                <asp:ListItem Value="">Select a program</asp:ListItem>
                <asp:ListItem Value="5000">Automotive ($5000)</asp:ListItem>
                <asp:ListItem Value="6000">Electrical ($6000)</asp:ListItem>
                <asp:ListItem Value="7000">Information Technology ($7000)</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:RequiredFieldValidator ID="programDdlValidator" runat="server" ControlToValidate="programDdl" ErrorMessage="Program not selected" InitialValue="0" CssClass="error">You must select a program</asp:RequiredFieldValidator>
            <asp:RadioButtonList ID="toolkitList" runat="server">
                <asp:ListItem Value="">Please Select a Toolkit</asp:ListItem>
                <asp:ListItem Value="5000">Mac Tool Box with complete set of automotive tools ($5000)</asp:ListItem>
                <asp:ListItem Value="400">Greenlee Electrician&#39;s Toolkit ($400)</asp:ListItem>
                <asp:ListItem Value="35">Belekin Computer Toolkit ($35)</asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="radioBtnValidator" runat="server" ControlToValidate="toolkitList" CssClass="error" ErrorMessage="No Toolkit Selected" InitialValue="0"></asp:RequiredFieldValidator>
            <asp:Label ID="Label1" runat="server" Text="Order Date:"></asp:Label>
            <br />
            <asp:TextBox ID="orderDateBox" runat="server" TextMode="Date"></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="noDateValidator" runat="server" ControlToValidate="orderDateBox" CssClass="error" ErrorMessage="No order date">You must select an order date</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="orderDateRangeValidator" runat="server" ControlToValidate="orderDateBox" CssClass="error" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Button ID="submitBtn" runat="server" Text="Submit" OnClick="submitBtn_Click" />
            <br />
            <asp:Label ID="CostLbl" runat="server" Text="Cost: "></asp:Label>
            <br />
            <asp:Label ID="deliveryDateLbl" runat="server" Text="Delivery Date: "></asp:Label>

        </div>
    </form>
</body>
</html>
