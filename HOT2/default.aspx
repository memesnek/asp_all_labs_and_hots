﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StyleSheet.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 56px">
            <asp:DropDownList ID="DdlInvoiceNum" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="InvoiceNumber" DataValueField="InvoiceNumber">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HalloweenConnection %>" SelectCommand="SELECT [InvoiceNumber], [CustEmail], [OrderDate], [Subtotal], [Shipping], [SalesTax], [Total] FROM [Invoices]"></asp:SqlDataSource>
        </div>
        <div style="height: 294px">
            <asp:Label ID="EmailLbl" runat="server" CssClass="Labels"></asp:Label>
            <asp:Label ID="DateLbl" runat="server" CssClass="Labels"></asp:Label>
            <asp:Label ID="SubTotalLbl" runat="server" CssClass="Labels"></asp:Label>
            <asp:Label ID="ShippingLbl" runat="server" CssClass="Labels"></asp:Label>
            <asp:Label ID="TaxLbl" runat="server" CssClass="Labels"></asp:Label>
            <asp:Label ID="TotalLbl" runat="server" CssClass="Labels"></asp:Label>
        </div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Contact Buyer" />
    </form>
</body>
</html>
