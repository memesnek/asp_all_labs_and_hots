﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DdlInvoiceNum.DataBind();
        }

        DataView productsTable = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        productsTable.RowFilter = string.Format("InvoiceNumber = '{0}'", DdlInvoiceNum.SelectedValue);
        DataRowView row = productsTable[0];

        EmailLbl.Text = "Email: " + row["CustEmail"].ToString();
        DateLbl.Text = "Date: " + row["OrderDate"].ToString();
        SubTotalLbl.Text = "Subtotal: " + row["Subtotal"].ToString();
        ShippingLbl.Text = "Shipping: " + row["Shipping"].ToString();
        TaxLbl.Text = "Tax: " + row["SalesTax"].ToString();
        TotalLbl.Text = "Total: " + row["Total"].ToString();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //DataView productsTable = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        //productsTable.RowFilter = string.Format("InvoiceNumber = '{0}'", DdlInvoiceNum.SelectedValue);
        //DataRowView row = productsTable[0];
        //HttpContext.Current.Session["Email"] = "Email: " + row["CustEmail"].ToString();

        HttpContext.Current.Session["Email"] = EmailLbl.Text;
        Response.Redirect("~/Page2.aspx");
    }
}