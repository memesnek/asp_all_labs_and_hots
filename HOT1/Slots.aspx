﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Slots.aspx.cs" Inherits="Slots" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="firstNumberLbl" runat="server" BorderStyle="Solid" Font-Size="XX-Large" Height="200px" Width="200px"></asp:Label>
            <asp:Label ID="secondNumberLbl" runat="server" BorderStyle="Solid" Font-Size="XX-Large" Height="200px" Width="200px"></asp:Label>
            <asp:Label ID="thirdNumberLbl" runat="server" BorderStyle="Solid" Font-Size="XX-Large" Height="200px" Width="200px"></asp:Label>
        </div>
        <div>
            <asp:Image ID="dogeCoin" runat="server" Height="300px" ImageUrl="~/Paycoins.jpg" Width="300px" />
            <asp:Label ID="luckySevenLabel" runat="server" Text="Lucky Seven"></asp:Label>
        </div>
        <div>
            <asp:Button ID="spinBtn" runat="server" OnClick="spinBtn_Click" Text="Spin" />
        </div>
    </form>
</body>
</html>
