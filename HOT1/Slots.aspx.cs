﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Slots : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dogeCoin.Visible = false;
        luckySevenLabel.Visible = false;
    }

    protected void spinBtn_Click(object sender, EventArgs e)
    {
        dogeCoin.Visible = false;
        luckySevenLabel.Visible = false;

        Random random = new Random();
        int number1 = random.Next(0, 10);
        int number2 = random.Next(0, 10);
        int number3 = random.Next(0, 10);
        firstNumberLbl.Text = number1.ToString();
        secondNumberLbl.Text = number2.ToString();
        thirdNumberLbl.Text = number3.ToString();

        if ((number1 == 7)||(number2 == 7)||(number3 == 7))
        {
            dogeCoin.Visible = true;
            luckySevenLabel.Visible = true;
        }
    }
}